# zdoom

## zdoom - on RetroPie Raspberry, Pi, RPI3b and RPI3b+, Multiplayer over LAN 



F4 and access the console tty1 (no X11)

![](medias/vanguard-mp.png)



## Raspberry PI rpi3b+  ( IP : 192.168...6) as Host (server and client)

"https://github.com/RetroPie/RetroPie-Setup/releases/download/4.5.1/retropie-4.5.1-rpi2_rpi3.img.gz"

pi setup and install zdoom. 

````
cd ; cd wads ;  /opt/retropie/ports/zdoom/zdoom -netmode 0 -port 5030 -host 2 -iwad doom2.wad -file Vanguard.wad 
````


## Raspberry PI rpi3b  ( IP : 192.168...22) as Client

"https://github.com/RetroPie/RetroPie-Setup/releases/download/4.5.1/retropie-4.5.1-rpi2_rpi3.img.gz"

pi setup and install zdoom. 


````
  cd ; cd wads ; mconfig -wadnet client 192.168....6 Vanguard.wad
````
or simply:

````
 /opt/retropie/ports/zdoom/zdoom +set fullscreen 1 -netmode 0 -join  192.168.....6  -port 5030 -iwad doom2.wad -file   Vanguard.wad  
````
